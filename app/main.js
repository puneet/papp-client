﻿requirejs.config({
    paths: {
        'text': '../lib/require/text',
        'durandal':'../lib/durandal/js',
        'plugins' : '../lib/durandal/js/plugins',
        'transitions' : '../lib/durandal/js/transitions',
        'knockout': '../lib/knockout/knockout-3.4.0',
        'bootstrap': '../lib/bootstrap/js/bootstrap',
        'jquery': '../lib/jquery/jquery-1.9.1',
        'knockout-validation': '../lib/knockout-validation/knockout.validation',
        'semantic-ui': '../lib/semantic-ui/semantic.min',
        'semantic-ui-calendar': '../lib/semantic-ui-calendar/calendar'
    },
    shim: {
        'bootstrap': {
            deps: ['jquery'],
            exports: 'jQuery'
        },
        'semantic-ui': {
            deps: ['jquery']
        },
        'semantic-ui-calendar': {
            deps: ['jquery']
        }
    }
});

define(['durandal/system', 'durandal/app', 'durandal/viewLocator', 'knockout', 'plugins/dialog', 'plugins/router', 'utils/durandal-extensions', 'knockout-validation', 'semantic-ui', 'semantic-ui-calendar'],  function (system, app, viewLocator, ko, dialog, router, durandalExtensions) {
    //>>excludeStart("build", true);
    system.debug(true);
    //>>excludeEnd("build");

    app.title = 'The Property App';

    app.configurePlugins({
        router:true,
        dialog: true
    });

    durandalExtensions.addDialogContextWithScrollBar();

    app.user = (function() {
        var isAuthenticated = false;
        return {
            isAuthenticated: isAuthenticated,
            details: null,
            authenticate: function(){
                isAuthenticated = true;
            }
        }
    })();

    router.guardRoute = function (instance, instruction) {
        console.log("instance .....");
        console.log(instance);
        console.log("instruction ......");
        console.log(instruction);
        if (app.user.isAuthenticated) {
            return true;
        } else {
            if (instance && !instruction.config.allowAnonymous){
                return 'login/' + instruction.fragment;
            }
            return true;
        }
    };

    app.start().then(function() {
        //Show the app by setting the root view model for our application with a transition.
        app.setRoot('components/shell', 'entrance');
    });

    ko.validation.init({
        insertMessages: true,
        messagesOnModified: true,
        decorateElementOnModified: true,
        decorateInputElement: true,
        errorElementClass: "error"
    });
});