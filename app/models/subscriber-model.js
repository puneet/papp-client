define(['knockout'], function(ko){

    return function SubscriberModel(arg) {
        arg = arg || {};
        var self = this;

        self.id = arg.id;
        self.name = ko.observable(arg.name).extend({ required: true, maxLength: 50 });

        self.jobTitle = ko.observable(arg.jobTitle);
        
        self.address1 = ko.observable(arg.address1);
        self.address2 = ko.observable(arg.address2);
        self.address3 = ko.observable(arg.address3);
        self.city = ko.observable(arg.city);
        self.state = ko.observable(arg.state);
        self.post_code = ko.observable(arg.postcode);
        //self.country = ko.observable(arg.country);
        
        self.phone = ko.observable(arg.phone);
        self.alt_phone = ko.observable(arg.alt_phone).extend({ maxLength: 50 });
        
        self.email = ko.observable(arg.email);
        self.website = ko.observable(arg.website);
        //self.referredBy = ko.observable(arg.referredBy); // which marketing campaign got this customer

        self.property_types = ko.observableArray();
        self.areas_covered = ko.observable();

        self.errors = ko.validation.group(self); //for ko validation

    };
});