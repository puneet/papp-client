define(['knockout'], function(ko){

    return function ActivityModel(arg) {
        arg = arg || {};
        var self = this;

        self.id = arg.id;
        self.scheduledFor = ko.observableArray(arg.scheduledFor ? JSON.parse(arg.scheduledFor) : undefined).extend({ required: true });;
        self.scheduledBy = ko.observable(arg.scheduledBy);
        self.scheduledWith = ko.observableArray(arg.scheduledWith ? JSON.parse(arg.scheduledWith) : undefined).extend({ required: true });;
        self.startDate = ko.observable(new Date(arg.startDate)).extend({ required: true });
        self.endDate = ko.observable(new Date(arg.endDate)).extend({ required: true });
        self.type = ko.observable(arg.type).extend({ required: true });
        self.title = ko.observable(arg.regarding).extend({ required: true, maxLength: 250 });
        self.description = ko.observable(arg.description).extend({ required: true, maxLength: 5000 });
        self.attachments = ko.observableArray(arg.attachments);
        self.createdOn = ko.observable(arg.createdOn);
        self.modifiedOn = ko.observable(arg.modifiedOn);

        self.startDate.subscribe(function(val){
            console.log('changed start date');
            console.log(val);
        });

        self.errors = ko.validation.group(self); //for ko validation

    };
});