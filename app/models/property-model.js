function PropertyModel(arg) {
    arg = arg || {};
    var self = this;
    self.name = ko.observable(arg.name);
    self.email = ko.observable(arg.email);
}
