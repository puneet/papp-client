define(['knockout'], function(ko){

    return function ContactModel(arg) {
        arg = arg || {};
        var self = this;

        self.id = arg.id;
        self.firstName = ko.observable(arg.first_name).extend({ required: true, maxLength: 50 });
        self.lastName = ko.observable(arg.last_name).extend({ required: true, maxLength: 50 });
        self.middleName = ko.observable(arg.middleName);
        self.nameSuffix = ko.observable(arg.nameSuffix);
        self.namePrefix = ko.observable(arg.namePrefix);
        self.nameTitle = ko.observable(arg.nameTitle);

        self.jobTitle = ko.observable(arg.jobTitle);
        
        self.address1 = ko.observable(arg.address1);
        self.address2 = ko.observable(arg.address2);
        self.address3 = ko.observable(arg.address3);
        self.city = ko.observable(arg.city);
        self.region = ko.observable(arg.region);
        self.postcode = ko.observable(arg.postcode);
        self.country = ko.observable(arg.country);
        
        self.phone = ko.observable(arg.phone);
        self.mobile = ko.observable(arg.mobile).extend({ required: true, maxLength: 50 });
        self.switchboard = ko.observable(arg.switchboard);
        
        self.email = ko.observable(arg.email);
        self.industry = ko.observable(arg.industry);
        self.website = ko.observable(arg.website);
        self.referredBy = ko.observable(arg.referredBy); // which marketing campaign got this customer

        self.types = ko.observableArray(arg.status);
        self.isInternalUser = ko.observable(arg.isInternalUser);

        self.createdOn = ko.observable(arg.createdOn);
        self.editedOn = ko.observable(arg.editedOn);
        self.importedOn = ko.observable(arg.importedOn);

        self.name = ko.computed(function(){
            return (self.firstName() || '') + ' ' + (self.lastName() || '');
        });

        self.errors = ko.validation.group(self); //for ko validation

    };
});