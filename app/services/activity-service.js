define(['plugins/http', './config'], function(http, config){

    return {
        get: function(){
            return http.get(config.baseUrl + 'activities').then(function(response){
                return response.data;
            });
        },
        addActivity: function(contact){
            return http.post(config.baseUrl + 'activities', contact).then(function(response){
                return response.data;
            });
        }
    }
});