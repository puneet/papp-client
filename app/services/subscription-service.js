define(['plugins/http', './config'], function(http, config){
    return {
        get: function(subscriberId){
            return http.get(config.baseUrl + 'subscribers/' + subscriberId + '/subscriptions').then(function(response){
                return response.data;
            });
        },
        getById: function(id){
            return http.get(config.baseUrl + 'subscribers/' + id).then(function(response){
                return response.data;
            });
        },
        addSubscriber: function(contact){
            return http.post(config.baseUrl + 'subscribers', contact).then(function(response){
                return response.data;
            });
        }
    }
});