define(['plugins/http', './config'], function(http, config){

    return {
        getContacts: function(){
            return http.get(config.baseUrl + 'contacts').then(function(response){
                return response.data;
            });
        },
        getUsers: function(){
            // todo: right now it is getting all contacts, replace by users
            return http.get(config.baseUrl + 'contacts').then(function(response){
                return response.data;
            });
        },
        addContact: function(contact){
            return http.post(config.baseUrl + 'contacts', contact).then(function(response){
                return response.data;
            });
        }
    }
});