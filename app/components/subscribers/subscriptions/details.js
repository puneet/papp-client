define(['knockout', 'models/subscription-model', 'durandal/app', 'plugins/router', 'services/subscriber-service'], function(ko, SubscriberModel, app, router, subscriberService) {
    return function() {
        var self = this;

        self.subscriberId;

        self.errors = ko.observableArray();
        self.subscriberName = ko.observable();

        self.activate = function(subscriberId) {

            self.subscriberId = subscriberId;
            self.subscription = new SubscriberModel();

            subscriberService.getById(subscriberId).done(function(response) {
                self.subscriberName(response.agencyName);
            });
        };

        self.save = function() {
            if (self.subscription.errors().length) {
                self.subscription.errors.showAllMessages();
            } else {

                var payload = {
                    subscription: Object.assign(self.subscription, {})
                };

                subscriberService.addSubscription(self.subscriberId, payload).done(function(result) {
                    router.navigate('subscribers/' + self.subscriberId);
                }).fail(function(response){
                    parseErrorsForDisplay(response, self.subscription);
                });

                function parseErrorsForDisplay(response, model) {
                    self.errors(Object.keys(JSON.parse(response.responseText).errors).map(function(key){
                        model[key].setError(errors[key][0]);
                        return key + ' ' + errors[key][0];
                    }));
                }
            }
        };

        self.compositionComplete = function(){

            $('#rangeStart').calendar({
                type: 'date',
                endCalendar: $('#rangeEnd'),
                onChange: function(date, text){
                    self.subscription.from_date(date);
                }
            });

            $('#rangeEnd').calendar({
                type: 'date',
                startCalendar: $('#rangeStart'),
                onChange: function(date, text){
                    self.subscription.to_date(date);
                }
            });
        };

    };
});