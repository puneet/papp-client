define(['knockout', 'services/subscriber-service'], function(ko, subscriberService){

    return function ViewModel(){

        var self = this;
        self.subscriberId;
        self.list = ko.observableArray();

        self.activate = function(subscriberId){
            self.subscriberId = subscriberId;
            subscriberService.getAllSubscriptions(subscriberId).then(function(response){
                self.list(response);
            });
        };
    }
})