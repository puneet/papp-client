define(['knockout', 'services/subscriber-service'], function(ko, subscriberService){

    return function ViewModel(){

        var self = this;

        self.list = ko.observableArray();

        self.activate = function(){
            subscriberService.get().then(function(response){
                self.list(response);
            });
        };
    }
})