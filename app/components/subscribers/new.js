define(['knockout', 'models/subscriber-model', 'durandal/app', 'plugins/router', 'services/subscriber-service'], function(ko, SubscriberModel, app, router, subscriberService){

    return function(){
        var self = this;

        self.errors = ko.observableArray();
        self.mode = ko.observable();

        self.activate = function(id){
            if (id.toLowerCase() === 'new') {
                self.mode("new");
                self.subscriber = new SubscriberModel();
                return;
            } else {
                self.mode("edit");
                return subscriberService.getById(id).then(function(response){
                    self.subscriber = new SubscriberModel(response);
                });
            }
        };

        self.save = function() {

            if (self.subscriber.errors().length) {
                self.subscriber.errors.showAllMessages();
            } else {

                var subscriberToSend = {
                    subscriber: Object.assign(
                        self.subscriber,
                        {
                            property_types: null
                        }
                    ),
                    user: {
                        id: app.user.details.id
                    }
                };

                subscriberService.addSubscriber(subscriberToSend).done(function(result){
                    console.log(result);
                    router.navigate('subscribers');
                }).fail(function(response){
                    var responseText = JSON.parse(response.responseText);
                    var errors = responseText.errors;
                    var errorList = Object.keys(errors).map(function(key){
                        console.log(key);
                        self.subscriber[key].setError(errors[key][0]);
                        return key + ' ' + errors[key][0];
                    });
                    // console.error(errorList);
                    self.errors(errorList);
                });
            }
        };
    }
});