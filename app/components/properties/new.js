define(['knockout', 'jquery'], function(ko, $){
    return function(){
        var self = this;
        self.dt = ko.observable();

        self.compositionComplete = function(){
            $('#dt').calendar({
                onChange: function (date, text) {
                    self.dt(date);
                    // console.log(typeof date);
                    // console.log(date);
                    // console.log(text);
                }
            });
        };

        self.save = function(){
            console.log(self.dt());
        }
    };
});