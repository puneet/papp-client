﻿define(['plugins/router', 'durandal/app', 'services/lookups-service', 'jquery'], function (router, app, lookupService, $) {
    return {
        router: router,

        activate: function () {

            router.map([{
                route: 'Home',
                title: 'Home',
                moduleId: 'components/dashboard/home',
                nav: true
            }, {
                route: 'Register',
                title: 'Register',
                moduleId: 'components/dashboard/register',
                nav: false
            }, {
                route: 'Login/*fragment',
                title: 'Login',
                moduleId: 'components/user-management/login',
                allowAnonymous: true,
                nav: false
            }, {
                route: 'Login',
                title: 'Login',
                moduleId: 'components/user-management/login',
                allowAnonymous: true,
                nav: false
            }, {
                route: 'Signup',
                title: 'Signup',
                moduleId: 'components/user-management/signup',
                allowAnonymous: true,
                nav: false
            }, {
                route: 'forgot-password',
                title: 'Forgot Password',
                moduleId: 'components/user-management/forgot-password',
                allowAnonymous: true,
                nav: false
            }, {
                route: '',
                title:'Dashboard',
                moduleId: 'components/dashboard/menu',
                nav: false
            }, {
                route: 'myProfile',
                title: 'My Profile',
                moduleId: 'components/dashboard/myProfile',
                nav: false 
            }, {
                route: 'devAgencyProfile',
                title: 'Developer/Agency Profile',
                moduleId: 'components/dashboard/devAgencyProfile',
                nav: false   
            }, {
                route: '',
                title:'Dashboard',
                moduleId: 'components/dashboard/menu',
                nav: true
            }, {
                route: 'subscribers',
                title: 'Subscribers',
                moduleId: 'components/subscribers/list',
                nav: true
            }, {
                route: 'subscribers/:id',
                title: 'View / Edit Agency',
                moduleId: 'components/subscribers/new'
            }, {
                route: 'subscribers/:id/subscriptions/:subscription_id',
                title: 'View / Edit Agency',
                moduleId: 'components/subscribers/subscriptions/details'
            }, {
                route: 'contacts',
                title: 'Contacts',
                moduleId: 'components/contacts/list',
                nav: true
            }, {
                route: 'contacts/new',
                title: 'New Contact',
                moduleId: 'components/contacts/new'
            }, {
                route: 'activities',
                title: 'Activities',
                moduleId: 'components/activities/list',
                nav: true
            }, {
                route: 'activities/new',
                title: 'New Activity',
                moduleId: 'components/activities/new'
            }, {
                route: 'properties',
                title: 'Properties',
                moduleId: 'components/properties/list',
                nav: true
            }, {
                route: 'lookups',
                title: 'Manage Lookups',
                moduleId: 'components/lookups/manage-lookups',
                nav: true
            }, {
                route: 'agency/new',
                title: 'Create Agency',
                moduleId: 'components/agency/new'
            }
            ]).buildNavigationModel();

            return lookupService.getLookupValues().then(function(lookupData){
                app.lookupData = lookupData;
                return router.activate()
            });
        }
    };
});