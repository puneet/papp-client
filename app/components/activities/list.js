define(['knockout', 'services/activity-service'], function(ko, activitysService){

    return function ViewModel(){

        var self = this;

        self.list = ko.observableArray();

        self.activate = function(){
            activitysService.get().then(function(response){
                self.list(response);
            });
        };
    }
})