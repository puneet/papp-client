define(['knockout', 'jquery', 'models/activity-model', 'durandal/app', 'plugins/router', 'services/activity-service', 'services/contact-service', 'lookups/activity-types'], function(ko, $, ActivityModel, app, router, activityService, contactsService, activityTypes){

    function User(arg) {
        var self = this;
        self.id = arg.id;
        self.name = arg.first_name + ' ' + arg.last_name;
        self.selected = ko.observable(false);
    }

    return function(){
        var self = this;

        self.errors = ko.observableArray();

        self.activityTypes = activityTypes.asArray;

        self.usersLookup = ko.observableArray();

        self.activate = function(){
            self.record = new ActivityModel();

            contactsService.getUsers().then(function(response){
                self.usersLookup(response.map(function(row){
                    return new User(row)
                }));
            });
        };

        self.selectContactsToScheduledWith = function(){
            app.showDialog(
                'components/contacts/select-contacts',
                {},
                'ScrollingBackground'
            ).then(function(response){
                self.record.scheduledWith(response.contacts);
            });
        };

        self.save = function() {

            var activity = self.record;

            var activityToSave = {
                activity: {
                    title: activity.title(),
                    description: activity.description(),
                    start_date: activity.startDate(),
                    end_date: activity.endDate(),
                    activity_type_id: activity.type(),
                    scheduled_with_contacts: activity.scheduledWith().map(function(item){
                        return item.id;
                    }),
                    scheduled_for_users:  activity.scheduledFor(),
                    scheduled_by_id: 1 //@@@ hardcoding for now
                }
            }

            console.log(activityToSave);

            activityService.addActivity(activityToSave).done(function(result){
                console.log(result);
                router.navigate('activities');
            }).fail(function(response){
                var responseText = JSON.parse(response.responseText);
                var errors = responseText.errors;
                var errorList = Object.keys(errors).map(function(key){
                    self.record[key].setError(errors[key][0]);
                    return key + ' ' + errors[key][0];
                });
                self.errors(errorList);
            });

            // if (self.contact.errors().length) {
            //     self.contact.errors.showAllMessages();
            // } else {

            //     var contactToSend = {
            //         contact: Object.assign(
            //             self.contact,
            //             {
            //                 first_name: self.contact.firstName,
            //                 last_name: self.contact.lastName,
            //                 types: null
            //             }
            //         )
            //     };

            //     activityService.addContact(contactToSend).done(function(result){
            //         console.log(result);
            //         router.navigate('contacts');
            //     }).fail(function(response){
            //         var responseText = JSON.parse(response.responseText);
            //         var errors = responseText.errors;
            //         var errorList = Object.keys(errors).map(function(key){
            //             self.contact[key].setError(errors[key][0]);
            //             return key + ' ' + errors[key][0];
            //         });
            //         // console.error(errorList);
            //         self.errors(errorList);
            //     });
            // }
        };

        self.compositionComplete = function(){

            $('#users').dropdown();
            $('#type').dropdown();

            $('#rangeStart').calendar({
                type: 'datetime',
                endCalendar: $('#rangeEnd'),
                onChange: function(date, text){
                    self.record.startDate(date);
                }
            });

            $('#rangeEnd').calendar({
                type: 'datetime',
                startCalendar: $('#rangeStart'),
                onChange: function(date, text){
                    self.record.endDate(date);
                }
            });
        };
    }
});