define(['jquery', 'knockout', 'services/lookups-service'], function($, ko, lookupService){

    return function(){

        var self = this;
        var allLookupValues = [];

        self.lookupTypes = ko.observableArray();
        self.lookupValues = ko.observableArray();
        self.selectedLookupType = ko.observable();
        self.newLookupValue = ko.observable();

        self.selectedLookupType.subscribe(populateLookupValues);

        function populateLookupValues(){
            var selectedLookupType = self.selectedLookupType();
            self.lookupValues(selectedLookupType
                ? allLookupValues.filter(function(lookupValue){
                    return lookupValue.lookup_type_id == selectedLookupType;
                  })
                : []
            );
        }

        self.activate = function(){
            return $.when(lookupService.getLookupTypes(), lookupService.getLookupValues())
             .then(function(typesResponse, valuesResponse){
                self.lookupTypes(typesResponse);
                allLookupValues = valuesResponse;
            });
        };

        self.addLookupValue = function(){

            var dataToSave = {
                lookup_value: {
                    name: self.newLookupValue(),
                    lookup_type_id: self.selectedLookupType()
                }
            };

            lookupService.addLookupValue(dataToSave).done(function(){
                lookupService.getLookupValues().then(function(response){
                    self.newLookupValue(null);
                    allLookupValues = response;
                    populateLookupValues();
                });
            });
        };

        self.binding = function(){
            $('#type').dropdown();
        };
    };

});