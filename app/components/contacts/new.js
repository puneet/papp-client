define(['knockout', 'models/contact-model', 'durandal/app', 'plugins/router', 'services/contact-service'], function(ko, ContactModel, app, router, contactService){

    return function(){
        var self = this;

        self.errors = ko.observableArray();

        self.activate = function(){
            self.contact = new ContactModel();
        };

        self.save = function() {

            if (self.contact.errors().length) {
                self.contact.errors.showAllMessages();
            } else {

                var contactToSend = {
                    contact: Object.assign(
                        self.contact,
                        {
                            first_name: self.contact.firstName,
                            last_name: self.contact.lastName,
                            types: null
                        }
                    )
                };

                contactService.addContact(contactToSend).done(function(result){
                    console.log(result);
                    router.navigate('contacts');
                }).fail(function(response){
                    var responseText = JSON.parse(response.responseText);
                    var errors = responseText.errors;
                    var errorList = Object.keys(errors).map(function(key){
                        self.contact[key].setError(errors[key][0]);
                        return key + ' ' + errors[key][0];
                    });
                    // console.error(errorList);
                    self.errors(errorList);
                });
            }
        };
    }
});