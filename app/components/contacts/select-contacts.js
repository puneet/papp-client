define(['knockout', 'plugins/dialog', 'services/contact-service'], function(ko, dialog,contactsService){

    function Contact(arg) {
        var self = this;
        self.id = arg.id;
        self.name = arg.first_name + ' ' + arg.last_name;
        self.selected = ko.observable(false);
    }

    return function ViewModel(){

        var self = this;

        self.list = ko.observableArray();

        self.activate = function(){
            contactsService.getContacts().then(function(response){
                self.list(response.map(function(row){
                    return new Contact(row)
                }));
            });
        };

        self.addSelectedContacts = function(){
            var selectedContacts = self.list().filter(function(contact){
                return contact.selected();
            });
            dialog.close(this, { contacts: selectedContacts });
        }
    }
})