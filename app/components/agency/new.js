define(['knockout', 'models/subscriber-model', 'durandal/app', 'plugins/router', 'services/subscriber-service'], function(ko, SubscriberModel, app, router, subscriberService){

    return function(){
        var self = this;

        self.errors = ko.observableArray();

        self.activate = function(id){
            if (!id) {
                self.model = new SubscriberModel();
                return;
            } else {
                return subscriberService.getById(id).then(function(response){
                    self.model = new SubscriberModel(response);
                });
            }
        };

        self.save = function() {

            if (self.model.errors().length) {
                self.model.errors.showAllMessages();
            } else {

                var subscriberToSend = {
                    subscriber: Object.assign(
                        self.model,
                        {
                            property_types: null
                        }
                    )
                };

                subscriberService.addSubscriber(subscriberToSend).done(function(result){
                    console.log(result);
                    router.navigate('subscribers');
                }).fail(function(response){
                    var responseText = JSON.parse(response.responseText);
                    var errors = responseText.errors;
                    var errorList = Object.keys(errors).map(function(key){
                        console.log(key);
                        self.model[key].setError(errors[key][0]);
                        return key + ' ' + errors[key][0];
                    });
                    // console.error(errorList);
                    self.errors(errorList);
                });
            }
        };
    }
});