define(['knockout', 'services/user-service'], function(ko, userService){

    function SignupModel(){
        var self = this;
        self.name = ko.observable();
        self.mobile = ko.observable();
        self.email = ko.observable();
        self.password = ko.observable();
        self.isAgent = ko.observable();
        self.isDeveloper = ko.observable();
        self.isLandlord = ko.observable();
        self.isBuyer = ko.observable();
        self.isTenant = ko.observable();
    }

    return function(){
        var self = this;

        self.model = new SignupModel();

        self.errors = ko.observableArray();

        self.signedUp = ko.observable(false);

        self.register = function(){

            self.errors([]);
            addUser();

            function addUser(){

                firebase.auth().createUserWithEmailAndPassword(self.model.email(), self.model.password()).then(function(user){
                    addFirebaseUserProfile();
                    addUserToBackend();
                    sendVerificationEmail(user);
                }).catch(function(error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;

                    self.errors.push(error.message);
                    // ...
                });

                function sendVerificationEmail(user){
                    user.sendEmailVerification().then(function() {
                        console.log("Verification email sent");
                        //navigateAway();
                        self.signedUp(true);
                    }, function(error) {
                        console.log("Error sending verification email");
                    });
                }

                function addFirebaseUserProfile(){
                    firebase.database().ref("/users")
                        .child(self.model.email().replace(".", "_dot_"))
                        .set(JSON.parse(ko.toJSON(self.model)));
                }

                function addUserToBackend(){

                    var userToSave = {
                        user: Object.assign(
                            JSON.parse(ko.toJSON(self.model)),
                            { first_name: self.model.name() })
                    };

                    userService.addUser(userToSave).done(function(result){
                        console.log(result);
                    }).done(function(response){
                        firebase.database().ref("/users")
                            .child(self.model.email().replace(".", "_dot_"))
                            .update({ id: response.id })
                    }).fail(function(response){
                        var responseText = JSON.parse(response.responseText);
                        var errors = responseText.errors;
                        var errorList = Object.keys(errors).map(function(key){
                            console.log(key);
                            self.model[key].setError(errors[key][0]);
                            return key + ' ' + errors[key][0];
                        });
                        // console.error(errorList);
                        self.errors(errorList);
                    });
                }
            }
        };
    };
});