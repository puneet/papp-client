define(['knockout', 'durandal/app', 'plugins/router', 'services/subscriber-service'], function(ko, app, router, subscriberService) {

    function LoginModel(){
        var self = this;
        self.email = ko.observable().extend({ required: true, maxLength: 50 });;
        self.password = ko.observable().extend({ required: true, maxLength: 50 });;
        self.errors = ko.validation.group(self); //for ko validation
    }

    return function(){
        var self = this;
        redirectToUrl = null;

        self.errors = ko.observableArray();

        self.firebaseUser = ko.observable();
        self.model = new LoginModel();
        self.userLoggedIn = ko.observable();
        self.emailNotVerified = ko.observable(false);

        self.activate = function(redirectToUrlParam){
            redirectToUrl = redirectToUrlParam;

            // firebase.auth().signOut().then(function() {
            //     console.log('Signed Out');
            // }, function(error) {
            //     console.error('Sign Out Error', error);
            // });

            firebase.auth().onAuthStateChanged(user => {

                self.firebaseUser(user);
                self.userLoggedIn(!!user);

                if (self.userLoggedIn()){
                    self.emailNotVerified(!user.emailVerified);
                    udateAppUser(user);

                    if (user.emailVerified){
                        if (false /* redirectToUrl */){
                            router.navigate(redirectToUrl);
                        } else {

                            firebase.database().ref("/users")
                                .child(app.user.details.email.replace(".", "_dot_"))
                                .once('value').then(function(snapshot){
                                    var firebaseUser = snapshot.val();

                                    app.user.details.id = firebaseUser.id;

                                    if (firebaseUser.isAgent) {
                                        takeAgentToTheAppropriatePage();
                                    } else if (firebaseUser.isDeveloper) {
                                        console.log("need decision from Manjul");
                                    } else if (firebaseUser.isLandlord) {
                                        console.log("need decision from Manjul");
                                    } else {
                                        // user is buyer or tenant
                                        takePublicUserToTheAppropriatePage();
                                    }
                                });
                        }
                    }
                }

                function takeAgentToTheAppropriatePage(user){
                    subscriberService.getForUser(1).done(function(result){
                        if (result.length){
                            // navigate to agent menu
                        } else {
                            router.navigate("#/subscribers/new");
                        }
                        console.log(result);
                        console.log("company for user");
                    });
                }

                function takePublicUserToTheAppropriatePage(){

                }

                function udateAppUser(user){
                    app.user.isAuthenticated = true;
                    app.user.details = {
                        name: user.displayName,
                        email: user.email,
                        photoUrl: user.photoURL,
                        provider: user.providerId,
                        uid: user.uid,
                        id: user.id,
                        refreshToken: user.refreshToken
                    };
                }
            });
        }

        self.login = function(type, event){

            if (type === 'google') {
                provider = new firebase.auth.GoogleAuthProvider();
            } else if (type === 'facebook') {
                provider = new firebase.auth.FacebookAuthProvider();
            } else if (type === 'twitter') {
                provider = new firebase.auth.TwitterAuthProvider();
            }

            firebase.auth().signInWithPopup(provider).then(function(result) {
                // The token for this session
                this.authToken = result.credential.accessToken;

                // The user object containing information about the current user
                this.user = result.user;

                // Set a class variable to true to state we are logged in
                this.userLoggedIn = true;
            }).catch(function(error) {
                console.log(error);
                // let errorCode = error.code;
                // let errorMessage = error.message;
                // let email = error.email;
                // let credential = error.credential;
            });
        };

        self.signInWithEmailAndPassword = function(){

            if (self.model.errors().length) {
                self.model.errors.showAllMessages();
                return;
            }

            firebase.auth().signInWithEmailAndPassword(self.model.email(), self.model.password()).catch(function(error) {
                console.warn(error);
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;

                self.errors([error.message]);
                // ...
            });
        }

        self.createUserWithEmailAndPassword = function(){
            firebase.auth().createUserWithEmailAndPassword(self.model.email(), self.model.password()).then(function(user){
                sendVerificationEmail(user);
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // ...
            });

            function sendVerificationEmail(user){
                user.sendEmailVerification().then(function() {
                    console.log("Verification email sent");
                    navigateAway();
                }, function(error) {
                    console.log("Error sending verification email");
                });
            }

            function navigateAway(){
                app.user.isAuthenticated = true;
                if (redirectToUrl){
                    router.navigate(redirectToUrl);
                } else {
                    router.navigate('#/home');
                }
            }
        }

        self.resendVerificationEmail = function(){
            self.firebaseUser().sendEmailVerification().then(function() {
                console.log("Verification email sent");
                //navigateAway();
            }, function(error) {
                console.log("Error sending verification email");
            });
        }
    };
});