define(['knockout', 'durandal/app', 'plugins/router'], function(ko, app, router) {

    return function(){
        var self = this;
        self.email = ko.observable().extend({ required: true, maxLength: 50 });
        
        self.errors = ko.validation.group(self); //for ko validation

        self.passwordResetEmailSent = ko.observable(false);

        self.submitForm = function(){
            if (self.errors().length) {
                self.errors.showAllMessages();
            } else {
                firebase.auth().sendPasswordResetEmail(self.email()).then(function() {
                    self.passwordResetEmailSent(true);
                }).catch(function(error) {
                    self.errors.push(error);
                });
            }
        };
    };

});