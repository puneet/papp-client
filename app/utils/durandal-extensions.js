﻿define(['jquery', 'knockout', 'plugins/dialog'], function ($, ko, dialog) {
    'use strict';

    return {
        addDialogContextWithScrollBar: function addDialogContextWithScrollBar() {
            dialog.addContext('ScrollingBackground', {
                addHost: function (theDialog) {
                    var uniqueId = (new Date()).valueOf(),
                        overlayId = 'modal-overlay-' + uniqueId,
                        contentWrapperId = 'modal-content-wrapper-' + uniqueId,
                        modalContentId = 'modal-content-' + uniqueId;
                    var body = $('body');
                    $('<div class="modal-overlay" id="' + overlayId + '">' +
                            '<div class="modal-table" id="' + contentWrapperId + '">' +
                                '<div class="modal-cell">' +
                                    '<div class="modal-content" id="' + modalContentId + '">' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>').appendTo(body);

                    theDialog.blockout = $('#' + overlayId).css({ 'z-index': dialog.getNextZIndex() }).get(0);
                    $('#' + contentWrapperId).css({ 'z-index': dialog.getNextZIndex() });
                    theDialog.host = $('#' + modalContentId).get(0);
                },
                removeHost: function (theDialog) {
                    setTimeout(function () {
                        ko.removeNode(theDialog.host);
                        ko.removeNode(theDialog.blockout);
                    }, 100);
                },
                attached: null
            });
        }
    };
});