define(['durandal/app'], function(app) {
    return {
        canActivate: function(){
            return app.user.isAuthenticated;
        }
    }
});