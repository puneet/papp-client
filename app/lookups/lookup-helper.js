define(['durandal/app'], function(app) {

    return {
        filterOutLookupType: function(lookupTypeId) {

            var lookupData = app.lookupData.filter(function(item) {
                return item.lookup_type_id == lookupTypeId;
            });

            var types = {};
            lookupData.forEach(function(row) {
                types[row.id] = row.name;
            });

            return types;
        }
    };

});