define(['./lookup-helper'], function(lookupHelper) {

    var lookupTypeId = 2;
    var types = lookupHelper.filterOutLookupType(lookupTypeId);

    return {
        asObject: types,
        asArray: Object.keys(types).map(function(key){
            return {
                id: key,
                caption: types[key]
            };
        })
    };

});